package br.com.medarot;

import robocode.*;
import java.awt.Color;

public class Medarot extends AdvancedRobot {
	boolean changeDirection = true;

	public void run() {
		setBodyColor(Color.black);
		setGunColor(Color.gray);
		setRadarColor(Color.red);

		setScanColor(Color.yellow);
		setBulletColor(Color.red);

		while (true) {
			if (changeDirection == true) {
				for (int walkRight = 0; walkRight < 4; walkRight++) {
					setAhead(100);
					setTurnRight(90);
					execute();

					ahead(100);
				}
				changeDirection = false;
			} else {
				for (int walkLeft = 4; walkLeft > 0; walkLeft--) {
					setAhead(100);
					setTurnLeft(90);
					execute();

					ahead(100);
				}
				changeDirection = true;
			}
		}
	}

	public void onScannedRobot(ScannedRobotEvent e) {
		double distance = e.getDistance();
		double angle = e.getBearing();

		if (distance < 200) {
			setAhead(100);
			setTurnRight(angle);
			setFire(3);
			execute();
		} else {
			setTurnGunRight(angle);
			setFire(1);
		}
	}

	public void onBulletHit(BulletHitEvent e) {
		fire(2);
	}

	public void onHitByBullet(HitByBulletEvent e) {
		setAhead(100);
		setTurnLeft(60);

	}

	public void onHitRobot(HitRobotEvent e) {
		back(100);
		turnLeft(90);
	}

	public void onHitWall(HitWallEvent e) {
		setBack(200);
		setTurnLeft(90);
		execute();
		ahead(50);
	}
}
