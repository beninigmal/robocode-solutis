![Solutis-2-1](/uploads/975839fbafe7f62d592134666097b469/Solutis-2-1.gif)
# __Talent Sprint Solutis__

## Sobre o Robocode

Robocode é um jogo de programação onde o objetivo é desenvolver um robô tanque para batalhar numa arena contra outros tanks, e as linguagens disponíveis são .NET e Java.

Segue abaixo alguns links da comunidade *Robocode*:
- [Robowiki](https://robowiki.net/)
- [Google Group](https://groups.google.com/forum/?fromgroups#!forum/robocode)

## Tecnologia utilizada
:coffee:  Java.

## O inicio de tudo 
![idade_da_pedra_lascada](/uploads/7b3a21cd42870aa3669f532b45ba1c36/idade_da_pedra_lascada.jpg)

De inicio foi necessário buscar mais informações sobre o projeto, ler a documentação e entender algumas funções básicas, então durante várias tentativas em arena, ficou mais claro algumas regras do jogo, onde percebi que a maior pontuação era alcançada principalmente por sobrevivência e a economia de energia permitia o robô pontuar melhor então busquei solução para algumas coisas que com frequência aconteciam:
- O Robô colidir muitas vezes na parede.
  - Para cada colisão o robô perde uma determinada quantidade de energia, então se o robô andasse para frente sem nenhuma condição de parada ou mudança de direção logo ele colidiria tantas vezes a sua energia permitisse.
- O Robô colidir com outros robôs.
  - Era comum nos testes colidir com outros robôs, e isso é até bem normal pelo que entendi, o grande problema era quando colidia entre um robô parado e uma parede, ou entre dois robôs, a energia esgotava quase que imediato.

## Objetivo do robô

O objetivo inicial era sobreviver o máximo possível evitando colisões, então uma série de testes foi feita, e muita pesquisa também e em meio a várias documentações oficiais e não oficiais, a que mais me ajudou foi o da [UFSC](https://www.gsigma.ufsc.br/~popov/aulas/robocode/eventos.html) o qual explicitou melhor como as coisas funcionam.

Após o robô consegui sobreviver reduzindo muito as colisões era hora de programar a ofensiva, mas ai percebi que o robô perdia as partidas muito rápido, mas percebi também que *desperdícios de tiros leva a derrota* e *acertos recuperam energia!*, então implementei uma condicional para atirar quando o robô identificasse a distancia miníma de 200px assim viraria o canhão e atiraria.

## Sobre o código

### Run
No código foi declarado no inicio que a classe Medarot extende da classe *AdvancedRobot* que é uma classe nativa da aplicação e nos permite fazer algumas coisas que a classe *robo* não faz. Em seguida foi declarado uma variável do tipo boleano *changeDirection* que fará o controle de direção dentro do robô, e em seguida no metódo *Run* serão definida as cores que são importadas do *java.awt.Color* e entra numa estrutura de repetição *while* que basicamente diz que enquanto não houver outro método em ação ele vai executar aquele bloco até que a partida termine, e para fazer o robô andar de forma dinâmica foi implementado 2 *for*, um para cada direção, sendo usada com o *execute()*, que faz o robô executar uma quantidade de códigos simultaneos, e em seguida uma alteração do estado da variável *changeDirection* para *false* no primeiro *for* e novamente *true*, para que alternasse as estruturas de repetição antes de reiniciar o *while*.

`

    while(true) {

    if (changeDirection == true){
			for(int walkRight = 0;walkRight < 4; walkRight++) {
				setAhead(100);
				setTurnRight(90);
				execute();
					
				ahead(100);
			}
				changeDirection = false;
	}
    else{
				
				for(int walkLeft = 4 ;walkLeft > 0; walkLeft--){
					setAhead(100);
					setTurnLeft(90);
					execute();
					
					ahead(100);
					//turnLeft(90);
				
				}
				changeDirection = true;
		}
			
	}
`

   


### onScannedRobot
Nesse metódo eu quis implementar a economia de energia atirando apenas em um determinado momento, pois uma grande parte da energia é gasta atirando.
Então logo de inicio declarei duas variáveis do tipo *double*, uma para obter a distância do robô inimigo captado no radar *distance*, e a outra o angulo *angle*. Elas são úteis para usar em uma estrutura condicional dizendo que se a distância do inimigo for **menor que 200px**, gire o canhão em direção ao angulo e atire o mais forte que puder no caso *fire(3)*, ou seja um tiro mais preciso, porém se isso não acontecer e o raio for **maior que 200px** tiros mais fracos serão disparados como no código abaixo.

`	 
 	
		if (distance < 200){		
			setAhead(100);
			setTurnRight(angle);
			setFire(3);
			execute();
		}else {
			setTurnGunRight(angle);
			setFire(1);
		}
`
### onBulletHit
Quando um alvo já foi atingido ele mandará no mesmo alvo um tiro mais fraco.

`
   
    public void onBulletHit(BulletHitEvent e) {
    	fire(2);
	}
  `
### onHitByBullet
Muda a direção quando é atingido.

`
	
    public void onHitByBullet(HitByBulletEvent e) {
		setAhead(100);
		setTurnLeft(60);
	}
  `

### onHitRobot
Quando colide com outro robô volta alguns pixels e muda a direção.

`
   
    public void	onHitRobot(HitRobotEvent e) {
		  back(100);
		  turnLeft(90);	
	  }
  `
### onHitWall
Quando colide com a parede, faz uma manobra para se afastar girando e andando para frente em seguida.

`

    public void onHitWall(HitWallEvent e) {
			setBack(200);
		   setTurnLeft(90);
		   execute();
		  ahead(50);
	}	
`

## Prós e contras

### Pró: 
Devido ao não desperdício de energia, o robô se torna durável o que ajuda a pontuar no parametro **survival.

### Contra:
O movimento é curto e algumas vezes passa muito tempo distante do oponente atirando de longe, e aos poucos vai gastando energia.

## Finalizando.

![war_machine](/uploads/5da44d70c86b23548bfda7d4ae200123/war_machine.jpg)

No final o que eu queria era ter a maior pontuação, e se fosse o vencedor seria excelente, e entre mais vários testes com 2 robôs já fornecidos pelo **robocode** e um outro que fiz anteriormente.

![vitória1](/uploads/aaac68f2c70e173da9239183a2adfeca/vitória1.png)

![vitória2](/uploads/24263e2b42d31b3978620d7a8641d8dc/vitória2.png)